evtc-watch
==========

Requirements
------------
* inotifywait
* bash 
* zip 
* curl (for upload feature)
* jq (for upload feature)
* evtc-watch-discord-messenger (compiled from this repo, for discord messaging after upload)

Usage
-----
```
$ evtc-watch --help
USAGE:
	evtc-watch [OPTIONS]

OPTIONS:
	--create-config		Creates/overwrites config with default values at
					${HOME}/.config/evtc-watch/config
	--auto-upload		Enables auto upload after zipping to dps.report (requires curl and jq).
	--auto-upload-filter	Regex which is matched against the absolute path of the evtc log before
				uploading. Only uploads the log if the regex matches.
	--link-log-file		Log file to which dps.report links are logged.
	--discord-link-message	Enables messaging the created dps.report link on discord. Requires
				evtc-watch-discord-messenger in $PATH. Requires configuration file
				with the options channel_id, discord_token and message_duration set.
	--directory		Sets the directory which should be watched for log creations (arcdps.cbtlogs)

```

Install
-------

AUR: https://aur.archlinux.org/packages/evtc-watch-git/

```
# After installing, create config
evtc-watch --create-config
# edit config to match your preferences
vim ${HOME}/.config/evtc-watch/config
# (opt) enable and start the evtc-watch daemon
systemctl --user enable --now evtc-watch
```

Configuration
-------------


```
# Strikes + Fractals auto upload
_strikes="Boneskinner|Whisper of Jormag|Fraenir of Jormag|Voice of the Fallen|Icebrood Construct"
_fractals="Skorvald the Shattered|Artsariiv|Arkk|MAMA|Nightmare Oratuss|Ensolyss of the Endless Torment"
auto_upload_filter="$_strikes|$_fractals"
```

Todo
----

* matrix/discord integration to post the dps.report link would be nicer
	* needs settings for where and what to post (participants, timespan, etc, => raidgrep?) or maybe just submit all logs to yourself

