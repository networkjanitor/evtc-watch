use std::env;

use chrono::prelude::*;
use serenity::model::*;
use serenity::prelude::*;
use structopt::StructOpt;

use log::{debug, error, info};

#[macro_use]
extern crate lazy_static;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "evtc-watch-discord-messenger",
    about = "Submitting a discord message to a channel, appending if message is within the timespan."
)]
struct Opt {
    channel_id: u64,
    link: String,
    timespan_in_hours: i64,
}

lazy_static! {
    static ref CONFIG: Opt = Opt::from_args();
}

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, ctx: Context, ready: gateway::Ready) {
        info!("{} is connected!", ready.user.name);
        let channel_id = id::ChannelId(CONFIG.channel_id);
        let mut _messages = channel_id
            .messages(&ctx.http, |retriever| retriever.limit(25))
            .unwrap();

        _messages.sort_by(|a, b| b.timestamp.cmp(&a.timestamp));
        _messages.retain(|msg| msg.is_own(&ctx));
        _messages
            .iter()
            .for_each(|msg| debug!("{:?}", Utc::now().signed_duration_since(msg.timestamp)));
        _messages.retain(|msg| {
            Utc::now().signed_duration_since(msg.timestamp)
                < chrono::Duration::hours(CONFIG.timespan_in_hours)
        });
        debug!("{:?}", _messages);
        match _messages.pop() {
            None => {
                if let Err(why) =
                    id::ChannelId(CONFIG.channel_id).say(&ctx.http, CONFIG.link.clone())
                {
                    error!("message send error: {:?}", why);
                    std::process::exit(1);
                }
            }
            Some(msg) => {
                if let Err(why) = msg.clone().edit(&ctx, |m| {
                    m.content(format!("{}\n{}", msg.content, CONFIG.link))
                }) {
                    error!("message edit error: {:?}", why);
                    std::process::exit(1);
                }
            }
        };
        std::process::exit(0);
    }
}

fn main() {
    env_logger::init();

    // Configure the client with your Discord bot token in the environment.
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let mut client = Client::new(&token, Handler).expect("Err creating client");

    if let Err(why) = client.start() {
        error!("Client error: {:?}", why);
    }
}
